const Footer = () => (
    <footer className="absolute w-full m-auto z-20 bg-transparent">
        <div className="relative text-center object-center content-center items-center m-auto my-10">
            <span className="text-sm font-normal">Made with ❤ by Nina, Inspired by Code Commerce - Code available on <a className="underline" target="_blank" rel="noreferrer" href="https://gitlab.com/Ninavuls/portfolio/">Gitlab</a></span>

        </div>
    </footer>
)

export default Footer;
