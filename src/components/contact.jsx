import React from "react";
import Lottie from 'lottie-react'
import animationData from '../assets/animation_cat.json'
import { useRef } from 'react'

const Contact = () => {
  return (
    <div id="contact" className="grid max-w-screen-lg grid-cols-1 gap-8 px-14 py-16 mx-auto rounded-lg md:grid-cols-2 md:px-20 lg:px-20 xl:px-20 bg-transparent text-gray-800">
      <div className="flex flex-col justify-between">
        <div className="space-y-2 md:px-16">
          <h2 className="text-4xl font-bold font-montserrat text-[#001b5e]">Let's talk!</h2>
          <div className="text-gray-600 font-lato">Open chat, open mind. Share anything!</div>
        </div>
        <Lottie animationData={animationData}/>
      </div>
      <form action="https://getform.io/f/b70343a8-7ff1-4ee7-bfcd-cecde5aa2bf5" method="POST" noValidate="" className="space-y-6">
        <div>
          <label htmlFor="name" className="text-sm font-lato">
            Full name
          </label>
          <input
            name="name"
            type="text"
            className="w-full p-3 rounded bg-transparent font-lato"
          />
        </div>
        <div>
          <label htmlFor="email" className="text-sm font-lato">
            Email
          </label>
          <input
            name="email"
            type="email"
            className="w-full p-3 rounded bg-transparent font-lato"
          />
        </div>
        <div>
          <label htmlFor="message" className="text-sm font-lato">
            Message
          </label>
          <textarea
            name="message"
            type="text"
            rows="3"
            className="w-full p-3 rounded bg-transparent font-lato"
          ></textarea>
        </div>
        <button
          type="submit"
          className="w-full p-3 text-sm font-bold tracki uppercase rounded bg-[#001b5e] text-white"
        >
          Send Message
        </button>
      </form>
    </div>
  );
};

export default Contact;
