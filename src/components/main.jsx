import React from "react";
import { TypeAnimation } from "react-type-animation";
import { FaLinkedinIn, FaGitlab } from "react-icons/fa";
import { BsPersonFill } from "react-icons/bs";
import cactus from '/src/assets/cactus.png';
import resume from '/src/assets/resume.pdf';

const Main = () => {
  const handleResumeClick = () => {
    window.open(resume, "_blank");
  };

  return (
    <div id="main">
      <img
        className="w-full h-screen object-cover scale-x-[-1] object-left "
        src={cactus}
        alt=""
      />
      <div className="w-full h-screen absolute top-0 left-0 bg-white/50">
        <div className="max-w-[700px] m-auto h-full w-full flex flex-col justify-center lg:items-start items-center">
          <h1 className="sm:text-5xl text-4xl font-bold text-gray-800 font-montserrat">
            I'm Nina
          </h1>
          <h2 className="flex sm:text-3xl text-2xl pt-4 text-gray-800 font-lato">
            I'm a
            <TypeAnimation
              sequence={[
                "Software Engineer",
                2000,
                "Literary Lover",
                2000,
                "Life Enthusiast",
                2000,
              ]}
              wrapper="div"
              cursor={true}
              style={{ fontSize: "1em", paddingLeft: "5PX" }}
              repeat={Infinity}
            />
          </h2>
          <div className="flex justify-between pt-6 max-w-[80px] w-full">
            <a
              href="https://www.linkedin.com/in/nina-kapanadze/"
              target="_blank"
              rel="noopener noreferrer"
            >
              <FaLinkedinIn className="cursor-pointer" size={20} />
            </a>
            <a
              href="https://gitlab.com/Ninavuls"
              target="_blank"
              rel="noopener noreferrer"
            >
              <FaGitlab className="cursor-pointer" size={20} />
            </a>
            <a onClick={handleResumeClick}>
              <BsPersonFill className="cursor-pointer" size={20} />
            </a>
          </div>
        </div>
      </div>
    </div>
  );
};

export default Main;
