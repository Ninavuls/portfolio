import reactImg from "../assets/react.png";

export const TechIcon = ({ name }) => {
  switch (name) {
    case "react":
      return (
        <div className="shadow-xl hover:scale-110 duration-500 rounded-xl font-lato">
          <img
            title="React.js"
            className="w-20 mx-auto"
            src="https://cdn.jsdelivr.net/gh/devicons/devicon/icons/react/react-original.svg"
          />
          <p>React</p>
        </div>
      );
    case "html":
      return (
        <div className="shadow-xl hover:scale-110 duration-500 rounded-xl font-lato">
          <img
            title="HTML"
            className="w-20 mx-auto"
            src="https://cdn.jsdelivr.net/gh/devicons/devicon/icons/html5/html5-original.svg"
          />
          <p>HTML 5</p>
        </div>
      );
    case "django":
      return (
        <div className="shadow-xl hover:scale-110 duration-500 rounded-xl font-lato">
          <img
            title="Django"
            className="w-20 mx-auto"
            src="https://cdn.jsdelivr.net/gh/devicons/devicon/icons/django/django-plain.svg"
          />
          <p>Django</p>
        </div>
      );
    case "css":
      return (
        <div className="shadow-xl hover:scale-110 duration-500 rounded-xl font-lato">
          <img
            title="CSS"
            className="w-20 mx-auto"
            src="https://cdn.jsdelivr.net/gh/devicons/devicon/icons/css3/css3-original.svg"
          />
          <p>CSS</p>
        </div>
      );
    case "redux":
      return (
        <div className="shadow-xl hover:scale-110 duration-500 rounded-xl font-lato">
          <img
            title="Redux"
            className="w-20 mx-auto"
            src="https://cdn.jsdelivr.net/gh/devicons/devicon/icons/redux/redux-original.svg"
          />
          <p>Redux</p>
        </div>
      );
    case "mysql":
      return (
        <div className="shadow-xl hover:scale-110 duration-500 rounded-xl font-lato">
          <img
            title="MySql"
            className="w-20 mx-auto"
            src="https://cdn.jsdelivr.net/gh/devicons/devicon/icons/mysql/mysql-original.svg"
          />
          <p>MySql</p>
        </div>
      );
    case "python":
      return (
        <div className="shadow-xl hover:scale-110 duration-500 rounded-xl font-lato">
          <img
            title="Python"
            className="w-20 mx-auto"
            src="https://cdn.jsdelivr.net/gh/devicons/devicon/icons/python/python-original.svg"
          />
          <p>Python</p>
        </div>
      );
    case "tailwind":
      return (
        <div className="shadow-xl hover:scale-110 duration-500 rounded-xl font-lato">
          <img
            title="Tailwind"
            className="w-20 mx-auto"
            src="https://cdn.jsdelivr.net/gh/devicons/devicon/icons/tailwindcss/tailwindcss-plain.svg"
          />
          <p>Tailwind</p>
        </div>
      );
    case "fastapi":
      return (
        <div className="shadow-xl hover:scale-110 duration-500 rounded-xl font-lato">
          <img
            title="FastAPI"
            className="w-20 mx-auto"
            src="https://cdn.jsdelivr.net/gh/devicons/devicon/icons/fastapi/fastapi-original.svg"
          />
          <p>FastAPI</p>
        </div>
      );
    case "javascript":
      return (
        <div className="shadow-xl hover:scale-110 duration-500 rounded-xl font-lato">
          <img
            title="JavaScript"
            className="w-20 mx-auto"
            src="https://cdn.jsdelivr.net/gh/devicons/devicon/icons/javascript/javascript-original.svg"
          />
          <p>JavaScript</p>
        </div>
      );
    case "postgresql":
      return (
        <div className="shadow-xl hover:scale-110 duration-500 rounded-xl font-lato">
          <img
            title="PostgresSQL"
            className="w-20 mx-auto"
            src="https://cdn.jsdelivr.net/gh/devicons/devicon/icons/postgresql/postgresql-original.svg"
          />
          <p>PostgreSQL</p>
        </div>
      );
    case "mongodb":
      return (
        <div className="shadow-xl hover:scale-110 duration-500 rounded-xl font-lato">
          <img
            title="MongoDB"
            className="w-20 mx-auto"
            src="https://cdn.jsdelivr.net/gh/devicons/devicon/icons/mongodb/mongodb-original.svg"
          />
          <p>MongoDB</p>
        </div>
      );
    default:
      return null;
  }
};
