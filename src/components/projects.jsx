import React from "react";
import { Link } from 'react-router-dom'
import travelThreads from '/src/assets/travelThreads.png';
import luxLane from '/src/assets/luxLane.png';
import conferences from '/src/assets/conferences.png';
import portfolio from '/src/assets/portfolio.png';

export const Projects = () => {
  return (
    <div
      id="project"
      className="max-w-[1280px] py-4 md:py-0 m-auto md:pl-20 md:py-16 p-4"
    >
      <h1 className="text-center font-montserrat font-bold text-4xl text-[#001b5e] ">
        Projects
      </h1>
      <hr className="m-auto self-center w-1/2 border-2 bg-blue-500 rounded-full mt-1"></hr>
      <div className="grid grid-cols-1 md:grid-cols-2 xl:grid-cols-2 relative px-2 my-2 md:px-14 md:pr-12 sm:p-2 py-10 md:p-4 gap-6 md:gap-4n md:py-10 py-10">
        <Link to='/projects/travel'>
        <div className="flex items-start col-span-1 md:col-span-1 xl:col-span-1 relative w-full h-full font-bold z-2">
          <div className="flex flex-col cursor-pointer items-start rounded w-full h-full overflow-hidden border-30px bg-transparent shadow-xl hover:-mb-14p border-8 border-custom-blue hover:border-t-30 hover:border-dark-blue
           hover:bg-cream duration-100">
            <div className="relative px-3 md:px-6 py-3 text-left hover:-am-14p">
              <div className="font-bold text-2xl text-[#001b5e] mb-2 z-20">
                Travel Threads
              </div>
              <p className="font-lato text-sm md:text-lg z-20 overflow-hidden">
                A packing assistant application that assists users in
                organizing items for their travels by considering
                destination-specific information and weather forecasts.
              </p>
            </div>
            <div className="flex items-end mt-auto relative px-0 lg:px-0 w-full max-h-96 overflow-hidden">
              <img
                src={travelThreads}
                alt="travelThreads.png"
                className="block w-full h-auto"
              />
            </div>
          </div>
        </div>
        </Link>
        <Link to="/projects/luxlane">
        <div className="flex items-start col-span-1 md:col-span-1 xl:col-span-1 relative w-full h-full font-bold z-2">
          <div className="flex flex-col cursor-pointer items-start rounded w-full h-full overflow-hidden border-30px bg-transparent shadow-xl hover:-mb-14p border-8 border-custom-blue hover:border-t-30 hover:border-dark-blue hover:bg-cream duration-100">
            <div className="relative px-3 md:px-6 py-3 text-left hover:-am-14p">
              <div className="font-bold text-2xl text-[#001b5e] mb-2 z-20">
                LuxLane
              </div>
              <p className="font-lato text-sm md:text-lg z-20 overflow-hidden">
                The car dealership management web application provides
                a comprehensive solution for efficiently managing
                vehicle sales, inventory, and service appointments.
              </p>
            </div>
            <div className="flex items-end mt-auto relative px-0 lg:px-0 w-full max-h-96 overflow-hidden">
              <img
                src={luxLane}
                alt="luxLane.png"
                className="block w-full h-auto"
              />
            </div>
          </div>
        </div>
        </Link>
        <Link to='/projects/conferencego'>
        <div className="flex items-start col-span-1 md:col-span-1 xl:col-span-1 relative w-full h-full font-bold z-2">
          <div className="flex flex-col cursor-pointer items-start rounded w-full h-full overflow-hidden border-30px bg-transparent shadow-xl hover:-mb-14p border-8 border-custom-blue hover:border-t-30 hover:border-dark-blue hover:bg-cream duration-100">
            <div className="relative px-3 md:px-6 py-3 text-left hover:-am-14p">
              <div className="font-bold text-2xl text-[#001b5e] mb-2 z-20">
                Conference Go!
              </div>
              <p className="font-lato text-sm md:text-lg z-20 overflow-hidden">
                Refactored a monolith into microservices to deploy new functionality.
              </p>
            </div>
            <div className="flex items-end mt-auto relative px-0 lg:px-0 w-full max-h-96 overflow-hidden">
              <img
                src={conferences}
                alt="conferences.png"
                className="block w-full h-auto"
              />
            </div>
          </div>
        </div>
        </Link>
        <Link to='/projects/portfolio'>
        <div className="flex items-start col-span-1 md:col-span-1 xl:col-span-1 relative w-full h-full font-bold z-2">
          <div className="flex flex-col cursor-pointer items-start rounded w-full h-full overflow-hidden border-30px bg-transparent shadow-xl hover:-mb-14p border-8 border-custom-blue hover:border-t-30 hover:border-dark-blue hover:bg-cream duration-100">
            <div className="relative px-3 md:px-6 py-3 text-left hover:-am-14p">
              <div className="font-bold text-2xl text-[#001b5e] mb-2 z-20">
                This Page!
              </div>
              <p className="font-lato text-sm md:text-lg z-20 overflow-hidden">
                A fully responsive React app styled with tailwind CSS.
              </p>
            </div>
            <div className="flex items-end mt-auto relative px-0 lg:px-0 w-full max-h-96 overflow-hidden">
              <img
                src={portfolio}
                alt="portfolio.png"
                className="block w-full h-auto"
              />
            </div>
          </div>
        </div>
        </Link>
      </div>
    </div>
  );
};

export default Projects;
