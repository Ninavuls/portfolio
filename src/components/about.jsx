import React from "react";
import { TechIcon } from "./techIcon";

const About = () => {
  return (
    <section
      id="about"
      className="max-w-[1280px] md:py-2 m-auto md:pl-20 p-4 py-3"
    >
      <div className="my-12 pb-12 md:pt-10 md:pb-48">
        <h1 className="text-center font-bold text-4xl text-[#001b5e] font-montserrat">
          About Me
          <hr className="m-auto self-center w-1/2 border-2 bg-blue-500 rounded-full mt-1"></hr>
        </h1>
        <div className="flex flex-col space-y-10 items-stretch justify-center align-top md:flex-row md:text-left md:p-4 md:space-y-0 md:space-x-10 py-10 md:py-10">
          <div className="md:w-1/2">
            <h1 className="text-center text-3xl font-bold mb-14 md:text-left text-[#001b5e] font-montserrat">
              Get to know me!
            </h1>
            <p className="font-lato text-lg">
              Hi, my name is Nina and I am a{" "}
              <span className="font-bold text-gray-800">
                {"highly ambitious"}
              </span>
              ,
              <span className="font-bold text-gray-800">
                {" self-motivated"}
              </span>
              , and
              <span className="font-bold text-gray-800">{" driven"}</span>{" "}
              software engineer based in New York City.
            </p>
            <br />
            <p className="font-lato text-lg">
              Born in Tbilisi, Georgia, I moved to the U.S. when I was just
              nine. I've always been a blend of diverse experiences, and my
              journey through life and into the tech world reflects that.
            </p>
            <br />
            <p className="font-lato text-lg">
              I pursued my BA in Psychology from Stony Brook University before
              venturing into the corporate sector with a role at a health
              organization. However, my keen interest in all things web dev led
              me to the Odin Project. Hungry for a deeper dive, I pursued
              rigorous training at Hack Reactor.
            </p>
            <br />
            <p className="font-lato text-lg">
              I'm deeply passionate about the ever-evolving world of technology
              and its boundless possibilities. This enthusiasm doesn't just stop
              at my professional pursuits; it's intertwined with a personal
              dedication to self-improvement. Outside of tech, I find grounding
              and inspiration in listening to science podcasts, weightlifting,
              diving into classical literature, and playing pickleball. Each of
              these activities is more than just a pastime; they fuel my drive,
              pushing me to constantly grow and challenge myself in every aspect
              of life.
            </p>
          </div>

          <div className="md:w-1/2">
            <h1 className="text-center text-3xl font-bold mb-6 md:text-left text-[#001b5e] font-montserrat">
              My Skills
            </h1>
            <div className="w-full grid grid-cols-2 sm:grid-cols-4 gap-3 text-center py-8">
              <TechIcon name="react" />
              <TechIcon name="html" />
              <TechIcon name="django" />
              <TechIcon name="css" />
              <TechIcon name="redux" />
              <TechIcon name="mysql" />
              <TechIcon name="python" />
              <TechIcon name="tailwind" />
              <TechIcon name="fastapi" />
              <TechIcon name="javascript" />
              <TechIcon name="postgresql" />
              <TechIcon name="mongodb" />
            </div>

          </div>
        </div>
      </div>
      <link
        href="https://fonts.googleapis.com/css2?family=Lato:wght@400;700&display=swap"
        rel="stylesheet"
      />
      <link
        href="https://fonts.googleapis.com/css2?family=Montserrat:wght@700&display=swap"
        rel="stylesheet"
      />
    </section>
  );
};

export default About;
