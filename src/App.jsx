import React from "react";
import { BrowserRouter, Routes, Route } from "react-router-dom";
import Sidenav from "./components/Sidenav";
import Main from "./components/Main";
import About from "./components/About";
import Projects from "./components/projects";
import Contact from "./components/Contact";
import Footer from "./components/footer";
import ProjectHandler from "./projectHandler";

function App() {
  return (
    <BrowserRouter>
      <Sidenav />
      <Routes>
        <Route
          path="/"
          element={
            <>
              <Main id="main" />
              <About id="about" />
              <Projects id="projects" />
              <Contact id="contact" />
              <hr className="mx-auto self-center w-full border-2 mt-7"></hr>

              <Footer id="footer" />
            </>
          }
        />
        <Route path="/projects/:projectId" element={<ProjectHandler />} />
      </Routes>
    </BrowserRouter>
  );
}

export default App;
