import React from 'react'


const Conference = () => {

  return (
    <div className='bg-[#6481d9] w-100 min-h-screen'>
        <div className='relative flow flow-wrap h-auto w-full text-left font-bold py-6 md:pl-20'>
            <div className='w-full px-3 md:px-6 py-2'>
                <div className='font-bold text-3xl text-[#f9feff] font-montserrat mb-4'>Conference Go</div>
                <hr className='mr-auto self-center w-1/2 border-2 bg-gray-200 rounded-full mt-1'></hr>
            </div>
        </div>
    <div className='relative px-3 md:px-6 py-3 text-left md:pl-24'>
        <p className=' font-lato text-lg md:text-xl z-20 overflow-hidden text-[#f9feff]'>
             Utilizes third-party API's for generating location-specific
             images and retrieving weather data.
             <br />
             Monitored message queue lengths to ensure timely processing of all tasks.
             <br />
             Code Available on <a className="underline" target="_blank" rel="noreferrer" href="https://gitlab.com/Ninavuls/conference-go">Gitlab!</a>
        </p>
    </div>
    </div>
  )
}

export default Conference;
