import React from 'react'
import salesrecord from '/src/assets/salesrecord.png';


const Luxlane = () => {

  return (
    <div className='bg-[#6481d9] w-100 min-h-screen'>
        <div className='relative flow flow-wrap h-auto w-full text-left font-bold py-6 md:pl-20'>
            <div className='w-full px-3 md:px-6 py-2'>
                <div className='font-bold text-3xl text-[#f9feff] font-montserrat mb-4'>LuxLane</div>
                <hr className='mr-auto self-center w-1/2 border-2 bg-gray-200 rounded-full mt-1'></hr>
            </div>
        </div>
    <div className='relative px-3 md:px-6 py-3 text-left md:pl-24'>
        <p className=' font-lato text-lg md:text-xl z-20 overflow-hidden text-[#f9feff]'>
             Django, React.js, RabbitMQ, PostgresSQL.
             <br />
             Compromised of three microservices: inventory, sales, service.
             Leveraged entity-relationship diagrams to visualize and communicate database
             structure, relationships, and constraints.
            E.g. salesrecord model has one-to-one relationship with AutomobileVO
            since each automobile has only one sales record. But has many-to-one
            relationship with salesperson and customer.
             <br />
             Polling mechanism was implemented to synchronize data across microservices.
             Application features robust data validation and error handling mechanisms.
             <br />
             Code Available on <a className="underline" target="_blank" rel="noreferrer" href="https://gitlab.com/Ninavuls/car-car">Gitlab!</a>
        </p>
    </div>
        <div className='flex relative mx-auto mt-5 flex-col md:flex-row h-auto pb-2 px-2 md:px-20 gap-2 md:gap-10'>
            <div className='block relative m-auto flex-col items-start gap-2'>
                <img src={salesrecord} alt="" className='block w-full h-auto top-0 mb-2' />
            </div>
        </div>
    </div>
  )
}

export default Luxlane;
