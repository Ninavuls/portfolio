import React from 'react'
import travelSign from '/src/assets/travel_sign.png';
import travelLists from '/src/assets/travel_lists.png';
import travelModal from '/src/assets/travel_modal.png';



const Travel = () => {

  return (
    <div className='bg-[#6481d9] w-100 min-h-screen'>
        <div className='relative flow flow-wrap h-auto w-full text-left font-bold py-6 md:pl-20'>
            <div className='w-full px-3 md:px-6 py-2'>
                <div className='font-bold text-3xl text-[#f9feff] font-montserrat mb-4'>Travel Threads</div>
                <hr className='mr-auto self-center w-1/2 border-2 bg-gray-200 rounded-full mt-1'></hr>
            </div>
        </div>
    <div className='relative px-3 md:px-6 py-3 text-left md:pl-24'>
        <p className=' font-lato text-lg md:text-xl z-20 overflow-hidden text-[#f9feff]'>
             PostgresSQL, FastAPI, Python, Redux.
             Application styled with Bootstrap.
             <br />
             Users select the country/state/city they're traveling to, which is seeded in the database,
             a call to Open Meteo is made, fetching the weather for those coordinates.
             <br />
             Users can then pack for their destination, according to the weather.
             And of course, their packed items will be displayed in a checklist which they can revisit
             any time.
             <br />
             Code Available on <a className="underline" target="_blank" rel="noreferrer" href="https://gitlab.com/team124532813/travel-threads">Gitlab!</a>
        </p>
    </div>
        <div className='flex relative mx-auto mt-5 flex-col md:flex-row h-auto pb-2 px-2 md:px-10 gap-2 md:gap-10'>
            <div className='block relative m-auto flex-col items-start gap-2'>
                <img src={travelSign} alt="" className='block w-full h-auto top-0 mb-2' />
                <img src={travelLists} alt='' className='block w-full h-auto top-0 mb-2'/>
            </div>
            <div className='flex relative flex-col items start'>
                <img src={travelModal} alt='' className='block w-full h-auto top-0'/>


            </div>

        </div>
    </div>
  )
}

export default Travel;
