import { useParams } from 'react-router-dom';
import Travel from './travel';
import Luxlane from './luxlane';
import Conference from './conference';
import Portfolio from './portfolio';

function ProjectHandler() {
    const { projectId } = useParams();

    switch (projectId) {
        case 'travel':
            return <Travel />;
        case 'luxlane':
            return <Luxlane />;
        case 'conferencego':
            return <Conference />;
        case 'portfolio':
            return <Portfolio />;
    }
}

export default ProjectHandler;
