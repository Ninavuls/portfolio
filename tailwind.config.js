/** @type {import('tailwindcss').Config} */
export default {
  content: ["./index.html", "./src/**/*.{js,ts,jsx,tsx}",],
  theme: {
    extend: {
      fontFamily: {
        'lato': ['lato', 'sans-serif'],
        'montserrat': ['montserrat', 'sans-serif'],
      },
      backgroundColor: {
        'transparent': 'transparent',
        'cream': '#f9feff',
      },
      borderColor: {
        'custom-blue': '#0f1c60',
        'dark-blue': '#6481d9',
      },
      margin: {
        '-14p': '-14px',
      },
      borderWidth: {
        '8': '8px',
        '30': '30px',
      },
      transitionDuration: {
        '100': '100ms',
      }
    }
  },
  variants: {
    extend: {
      marginBottom: ['hover'],
      borderWidth: ['hover'],
      borderColor: ['hover'],
      backgroundColor: ['hover']
    },
    borderWidth: ['hover'],
  },
  plugins: [
    require('@tailwindcss/forms'),
  ],
}
